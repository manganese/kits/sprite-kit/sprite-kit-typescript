// Interfaces
import UnityVector2 from '@manganese/serialization-kit/interfaces/UnityVector2';

export default class Spritesheet {
  // Image data
  public imageData: ImageData;

  // Frame size
  public frameWidth: number;
  public frameHeight: number;

  public get frameSize(): UnityVector2 {
    return {
      x: this.frameWidth,
      y: this.frameHeight,
    };
  }

  public set frameSize(value: UnityVector2) {
    this.frameWidth = value.x;
    this.frameHeight = value.y;
  }

  // Frame count
  public frameCount: number;

  // Frames
  public getFrameAtIndex(index: number): ImageData {
    return null; // TODO
  }

  public getFrameRange(startIndex: number, endIndex: number): ImageData {
    return null; // TODO
  }

  // Constructor
  public Spritesheet(
    imageData: ImageData,
    spriteSize: UnityVector2,
    frameSize: UnityVector2,
    frameCount: number
  ) {
    this.imageData = imageData;
  }
}
